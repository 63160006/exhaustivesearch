/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.exhaustivesearch;

import java.util.ArrayList;

/**
 *
 * @author tud08
 */
public class Node {

    private ArrayList<Integer> subP1 = new ArrayList<Integer>();
    private ArrayList<Integer> subP2 = new ArrayList<Integer>();

    public Node(ArrayList<Integer> arrPair1, ArrayList<Integer> arrPair2) {
        this.subP1 = arrPair1;
        this.subP2 = arrPair2;
    }

    public ArrayList<Integer> getArrayP1() {
        return subP1;
    }

    public void setArrayP1(ArrayList<Integer> arrayP1) {
        this.subP1 = arrayP1;
    }

    public ArrayList<Integer> getArrayP2() {
        return subP2;
    }

    public void setArrayP2(ArrayList<Integer> arrayP2) {
        this.subP2 = arrayP2;
    }
}

