/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.exhaustivesearch;

import java.util.ArrayList;

/**
 *
 * @author tud08
 */
public class ExhaustiveSearch {
    private int[] input;
    private ArrayList<Node> arrayP;
    static int indexArray = 0;
    static ArrayList<ArrayList<Integer>> p1 = new ArrayList<ArrayList<Integer>>();
    static ArrayList<ArrayList<Integer>> p2 = new ArrayList<ArrayList<Integer>>();

    public ExhaustiveSearch(int[] input) {
        this.input = input;
    }

    public ExhaustiveSearch() {
    }
    
    public void addInput() {
        ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<Integer> temp2 = new ArrayList<>();

        for (int o = 0; o < input.length; o++) {
            temp.add(input[o]);
        }
        for (int i = 0; i < input.length; i++) {
            temp2.add(input[i]);
        }

        int array[] = input;
        int n = array.length;

        for (int i = input.length; i >= 0; i--) {
            showCombination(array, n, i);
        }

        indexArray = 1;
        p2.add(0, temp2);
        for (int j = 1; j < p1.size(); j++) {
            temp = new ArrayList<>();
            for (int i = 0; i < input.length; i++) {
                temp.add(input[i]);
            }
            for (int k = 0; k < p1.get(j).size(); k++) {
                for (int u = 0; u < temp.size(); u++) {
                    if (temp.get(u) == p1.get(j).get(k)) {
                        temp.remove(u);
                        u--;
                    }
                }
            }
            p2.add(indexArray, temp);
            indexArray++;
        }
    }
    
    public void getA() {
        for (int i = 0; i < p1.size(); i++) {
            for (int j = 0; j < p1.get(i).size(); j++) {
                System.out.print(p1.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }
    
    static void showCombination(int arr[], int n, int r) {
        int data[] = new int[r];
        combinationUtil(arr, data, 0, n - 1, 0, r);
    }
    
    public void process() {
        for (int i = 0; i < p1.size(); i++) {
            if (p1.contains(p2.get(i))) {
                p2.remove(i);
                p1.remove(i);
            }
        }
        for (int i = 0; i < p1.size(); i++) {
            int sumA = 0;
            int sumB = 0;
            for (int j = 0; j < p1.get(i).size(); j++) {
                sumA += p1.get(i).get(j);
            }
            for (int j = 0; j < p2.get(i).size(); j++) {
                sumB += p2.get(i).get(j);
            }
            if (sumA == sumB) {
                System.out.println(p1.get(i) + " " + p2.get(i));
            }
        }
    }

    public void getB() {
        for (int i = 0; i < p2.size(); i++) {
            for (int j = 0; j < p2.get(i).size(); j++) {
                System.out.print(p2.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }
    
    static void combinationUtil(int arr[], int data[], int start,
            int end, int index, int r) {
        ArrayList<Integer> temp = new ArrayList<>();
        if (index == r) {
            for (int j = 0; j < r; j++) {
                temp.add(data[j]);
            }
            p1.add(indexArray, temp);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= r - index; i++) {
            data[index] = arr[i];
            combinationUtil(arr, data, i + 1, end, index + 1, r);
        }
    }
}
