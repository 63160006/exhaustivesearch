/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.exhaustivesearch;

/**
 *
 * @author tud08
 */
public class TestExhaustiveSearch {
    public static void main(String[] args) {
        int[] x = {1, 3, 5, 7, 10, 4, 8};
        showInput(x);
        ExhaustiveSearch exs = new ExhaustiveSearch(x);
        exs.addInput();
    }
    
    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
        System.out.println("");
    }

}
